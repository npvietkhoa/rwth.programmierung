/**
 * ZinseszinsRechner
 */
public class ZinseszinsRechner {

    double startBetrag;
    double zinssatz;
    double zielBetrag;
    int zeit = 0;
    double gesamtBetrag;

    void getStartBetragAndZinssatz() {
        this.startBetrag = SimpleIO.getDouble("Bitte geben Sie den Startbetrag ein.");
        //Vermeiden, dass die Nutzer die negative Betrag hinzufugt.
        while (startBetrag > Double.MAX_VALUE || startBetrag <= 0) {
            this.startBetrag = SimpleIO.getDouble("Bitte geben Sie den Startbetrag ein.\n " +
                                    "Der eingegebene Betrag war zu viel oder zu wenig.");
        }
        this.gesamtBetrag = startBetrag;

        this.zinssatz = SimpleIO.getDouble("Bitte geben Sie den Zinssatz als Prozentwert ein.");
        //Zwingen, die Zinssatz groesser oder gleich als 0 einzugeben
        while (zinssatz < 0) {
            this.zinssatz = SimpleIO.getDouble("Bitte geben Sie den Zinssatz als Prozentwert ein.\n " + 
                                    "Der Zinssatz muss ab 0 sein.");
        }
    }

    void berechnenZielBetrag() {

        this.zeit = SimpleIO.getInt("Bitte geben Sie den Zeit als Jahr ein.");
        //Vermeiden, dass die Nutzer negativen Zeit oder groesser als max von Integer.
        while (this.zeit < 0 || this.zeit > Integer.MAX_VALUE) {
            this.zeit = SimpleIO.getInt("Bitte geben Sie den Zeit als Jahr ein. \n" + 
                                    "Der eingegebe Zeit war zu viel oder zu wenig.");
        }
        for (int i = 0; i < this.zeit; i++) {
            // Wenn Zinssatz = 0%, das bedeutet die gesamt Betrag aendert sich nicht. 
            // Dann break sofort die Schleife.
            if (this.zinssatz == 0) {
                break;
            }
            //Falls alles erfuellt, rechnet gesamt betragt mit formular
            gesamtBetrag = gesamtBetrag * (1 + this.zinssatz / 100); 
        }

        String outputContent = "Bei einem Zinssatz von " + this.zinssatz + "% und einem Startbetrag von " +
                                + this.startBetrag + " hat man nach 2 Jahren " + this.gesamtBetrag + " gespart";

        SimpleIO.outputEnd(outputContent, "Ergebnis");
    }

    void berechnenZeit() {
        this.zielBetrag = SimpleIO.getDouble("Bitte geben Sie den Zielbetrag ein.");
        while (this.zielBetrag < 0 || this.zielBetrag < this.startBetrag) {
            this.zielBetrag = SimpleIO.getDouble("Bitte geben Sie den Zielbetrag ein. \n" + 
                                        "Der Zielbetrag muss groesse als Startbetrag sein.");
        }


        while (gesamtBetrag < zielBetrag) {
            //Falls Zinssatz == 0, bedeutet das, dass die gesammt Betrag aendert sich nicht.
            //zeig sofort gesamt Betrag und break Schleife mit outputEnd()
            if (this.zinssatz == 0) {
                SimpleIO.outputEnd("Sie kriegen immer " + this.gesamtBetrag, "Ergebnis");
            }
            gesamtBetrag = gesamtBetrag * (1 + zinssatz / 100);
            this.zeit += 1;
        }

        String outputString = "Es dauert " + this.zeit + " Jahre bei einem Zinssatz von " + this.zinssatz + 
                    "%, um von " + this.startBetrag + " auf den Betrag " + this.zielBetrag + 
                    " zu sparen . Nach dieser Zeit hat man " + this.gesamtBetrag + ".";
        
        SimpleIO.outputEnd(outputString, "Ergebniss");
    }

    void anfangen() {
        getStartBetragAndZinssatz();
        String outputFrage = "Bitte waehlen Sie aus:\n" + 
            "Ziel: Berechnet die Zeit , bis ein gegebener Betrag angespart wurde.\n" +
            "Zeit: Berechnet den Betrag , der nach einer gegebenen Zeit angespart wurde.\n";
        String waehl = SimpleIO.getString(outputFrage);
        if (waehl.equals("Ziel")) {
            berechnenZeit();
        } else if (waehl.equals("Zeit")) {
            berechnenZielBetrag();
        } else {
            SimpleIO.outputEnd("Es gibt keine solche Waehl", "Fehler");
        }
    }

}