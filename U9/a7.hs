-- Aufgabe 7

data Optional a = Empty | Present a deriving Show

-- Teilaufgabe a
mapOptional :: (a -> b) -> Optional a -> Optional b
mapOptional _ Empty = Empty
mapOptional f (Present a) = Present (f a)

-- Teilaufgabe b
filterOptional :: (a -> Bool) -> Optional a -> Optional a
filterOptional _ Empty = Empty
filterOptional f (Present a) | f a  = Present a
                             | otherwise  = Empty

-- Teilaufgabe c
foldOptional :: (a -> b) -> b -> Optional a -> b
foldOptional _ defaultVal Empty = defaultVal
foldOptional f defaultVal (Present a) = f a

-- Teilaufgabe d
data Product = Article String Int deriving Show

isHumanEatable :: Product -> Bool
isHumanEatable (Article food price) | food == "Dog Food" = False
                                    | otherwise  = True

adjustPrice :: Product -> Product
adjustPrice (Article food price) | price < 1000 = Article food (price * 2)
                                 | otherwise = Article food price

stringify :: Product -> String
stringify (Article food price) = "The article named '" ++ food ++ "' cost " ++ show price ++ " Cent."

-- Teilaufgabe e

filterHumanEatable :: Product -> Optional Product
filterHumanEatable product = filterOptional isHumanEatable (Present product)

adjustPriceO :: Optional Product -> Optional Product
adjustPriceO = mapOptional adjustPrice

stringifyO :: Optional Product -> String
stringifyO = foldOptional stringify "This article is unavailable"

toPriceTag :: Product -> String
toPriceTag product = stringifyO (adjustPriceO (filterHumanEatable (product)))