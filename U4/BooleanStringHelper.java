/**
 * BooleanStringHelper
 */
public class BooleanStringHelper {

    /**
     * Umwandeln String Werte, der boolesche Eigentschaft, zu Boolesche Array
     * @param booleanStr String der Booleschen Werte
     * @param charTrueValue Buchstabe, der aequivalent true in Boolesche Array
     * @return Passende boolesche Array von vorgegeben String 
     */
    static boolean[] parse(String booleanStr, char charTrueValue) {
        if (booleanStr == null) {
            return null;
        }
        boolean[] booleanArr = new boolean[booleanStr.length()];
        for (int i = 0; i < booleanStr.length(); i++) {
            booleanArr[i] = booleanStr.charAt(i) == charTrueValue;
        }
        return booleanArr;
    }

    /**
     * Umwandeln boolesche Array zu ein String mit den aequivalenten Buchstaben.
     * @param booleanArr Vorgegebenes boolesches Array.
     * @param trueCharValue Buchstabe, die in String hinzugefuegt wird, ist äquivalent zu true in booleschen Array.
     * @param falseCharValue Buchstabe, die in String hinzugefuegt wird, ist äquivalent zu false in booleschen Array.
     * @return Aequivalente String von booleschem Array, die aus vorgegebenen Buchstaben besteht.
     */
    static String stringify(boolean[] booleanArr, char trueCharValue, char falseCharValue) {
        StringBuilder builder = new StringBuilder("");
        for (boolean b : booleanArr) {
            builder.append((b == true) ? trueCharValue : falseCharValue);
        }
        return builder.toString();
    }
}