import java.math.BigDecimal;
/**Aufgabe 8c
 *Klasse fuer die Gerade
 */
public class Gerade {
    final Punkt p1;
    final Punkt p2;

    /**Aufgabe 8c:
     * Konstruktor um ein Gerade durch 2 gegebene Punkte zu erstellen
     * @param p1 Punkte 1
     * @param p2 Punkte 2
     */
    Gerade(Punkt p1, Punkt p2) {
        if (p1.equals(p2)) {
            System.out.println("Ungueltige Eingabe");
            this.p1 = null;
            this.p2 = null;
        } else {
            if (p1.x.compareTo(p2.x) < 0) {
                this.p1 = p1;
                this.p2 = p2;
            } else if (p1.x.compareTo(p2.x) > 0) {
                this.p1 = p2;
                this.p2 = p1;
            } else {
                if (p1.y.compareTo(p2.y) < 0) {
                    this.p1 = p1;
                    this.p2 = p2;
                } else {
                    this.p1 = p2;
                    this.p2 = p1;
                }
            }
        }
    }

    /**Aufgabe 8c:
     * Ergibt String, der ueber die Gerade beschreiben kann
     * @return String beschreibt die Gerade
     */
    public String toString() {
        if (this.p1 != null && this.p2 != null) {
            return "Gerade durch " +
                    this.p1.toString() +
                    " und " +
                    this.p2.toString();
        } else {
            return "Keine Gerade";
        }
    }

    /**Aufgabe 8d:
     * Pruefen, ob ein Punkt auf diese Gerade ist
     * @param p0 die Punkte
     * @return Punkt liegt an Gerade oder nicht
     */
    boolean enthaelt(Punkt p0) {
        if (p1.equals(p0) || p2.equals(p0)) {
            return true;
        }
        // k1 ist Quotient zwischen (p0.x - p1.x) und (p1.x - p2.x)
        BigDecimal k1 = (this.p1.x.add(p0.x.negate())).divide(this.p1.x.add(this.p2.x.negate()));
        // k2 ist Quotient zwischen (p0.y - p1.y) und (p1.y - p2.y)
        BigDecimal k2 = (this.p1.y.add(p0.y.negate())).divide(this.p1.y.add(this.p2.y.negate()));

        return BigDecimalUtility.equalValues(k1, k2);
    }

    /**Aufgabe 8d:
     * Pruefen, ob ein Punkt liegt inzwischen 2 Punkte, den die Gerade durchlauft.
     * @param p0 gepruefter Punkt.
     * @return ob gepruefter Punkt liegt inzwischen 2 Punkte, den die Gerade durchlauft.
     */
    protected boolean zwischenp1p2(Punkt p0) {
        BigDecimal abstandp1p0 = p1.abstand(p0);
        BigDecimal abstandp2p0 = p2.abstand(p0);
        BigDecimal abstandp1p2 = p1.abstand(p2);
        return this.enthaelt(p0) && BigDecimalUtility.equalValues(abstandp1p0.add(abstandp2p0), abstandp1p2);
    } 

    /**Aufgabe 8d:
     * Pruefen, ob ein Punkt liegt vor dem ersten Punkt der Gerade
     * @param p0 gepruefter Punkt
     * @return gefruefter Punkt liegt vor dem ersten Punkt der Gerade
     */
    protected final boolean vorp1(Punkt p0) {
        if (this.enthaelt(p0) && !this.zwischenp1p2(p0)) {
            return this.p1.abstand(p0).compareTo(this.p2.abstand(p0)) < 0;
        }
        return false;
    }

    /**Aufgabe 8d:
     * Pruefen, ob ein Punkt liegt hinter dem zweiten Punkt der Gerade
     * @param p0 gepruefter Punkt
     * @return true wenn gefruefter Punkt hinter dem zweiten Punkt der Gerade liegt
     */
    protected final boolean hinterp2(Punkt p0) {
        if (this.enthaelt(p0)) {
            return !this.zwischenp1p2(p0) && !this.vorp1(p0);
        }
        return false;
    }

    /**Aufgabe 8e:
     * Zeigt die Gleicheit von ein Objekt mit this Gerade
     * @param obj ein Objekt
     * @return true wenn gegebenes Objekt this Gerade gleich ist
     */
    public boolean equals(Object obj) {
        if (this.getClass().equals(obj.getClass())) {

            Gerade objGerade = (Gerade) obj;

            // Festellen, ob
            if (objGerade.p1 == null || objGerade.p2 == null || this.p1 == null || this.p2 == null) {

                return false;
            }

            return this.enthaelt(objGerade.p1) && this.enthaelt(objGerade.p2);
        }
        return false;
    }
}
